#include "red_black_tree.hpp"

namespace iox
{
namespace cxx
{

template <typename Type, uint64_t Capacity>
inline red_black_tree<Type, Capacity>::red_black_tree()
{
    for (uint64_t i = 0; i < Capacity; ++i)
    {
        m_indices.push(Capacity - i - 1U);
    }
}

template <typename Type, uint64_t Capacity>
inline typename red_black_tree<Type, Capacity>::Node& red_black_tree<Type, Capacity>::get_node(const uint64_t index)
{
    return reinterpret_cast<Node&>(m_nodes[index]);
}

template <typename Type, uint64_t Capacity>
inline void red_black_tree<Type, Capacity>::left_rotate(const uint64_t nodeIndex)
{
    auto& node = get_node(nodeIndex);
    if (!node.has_right_subtree())
    {
        return;
    }

    auto rightNodeIndex = node.right;
    auto& rightNode = get_node(rightNodeIndex);
    node.right = rightNode.left;

    if (rightNode.has_left_subtree())
    {
        auto& newParent = get_node(rightNode.left);
        newParent.parent = nodeIndex;
    }

    rightNode.parent = node.parent;
    if (!node.has_parent())
    {
        m_root = rightNodeIndex;
    }
    else
    {
        auto& parent = get_node(node.parent);
        if (parent.left == nodeIndex)
        {
            parent.left = rightNodeIndex;
        }
        else
        {
            parent.right = rightNodeIndex;
        }
    }

    rightNode.left = nodeIndex;
    node.parent = rightNodeIndex;
}

template <typename Type, uint64_t Capacity>
inline void red_black_tree<Type, Capacity>::right_rotate(const uint64_t nodeIndex)
{
    auto& node = get_node(nodeIndex);
    if (!node.has_left_subtree())
    {
        return;
    }

    auto leftNodeIndex = node.left;
    auto& leftNode = get_node(leftNodeIndex);
    node.left = leftNode.right;

    if (leftNode.has_right_subtree())
    {
        auto& newParent = get_node(leftNode.right);
        newParent.parent = nodeIndex;
    }

    leftNode.parent = node.parent;
    if (!node.has_parent())
    {
        m_root = leftNodeIndex;
    }
    else
    {
        auto& parent = get_node(node.parent);
        if (parent.right == nodeIndex)
        {
            parent.right = leftNodeIndex;
        }
        else
        {
            parent.left = leftNodeIndex;
        }
    }

    leftNode.right = nodeIndex;
    node.parent = leftNodeIndex;
}

template <typename Type, uint64_t Capacity>
inline bool red_black_tree<Type, Capacity>::insert(const Type& value)
{
    if (m_indices.empty())
    {
        return false;
    }

    auto newIndex = m_indices.top();

    if (m_root == INVALID_INDEX)
    {
        new (m_nodes[newIndex]) Node{INVALID_INDEX, INVALID_INDEX, INVALID_INDEX, value, Color::BLACK};
        return true;
    }

    uint64_t nodeIndex = m_root;
    while (true)
    {
        auto& node = get_node(nodeIndex);

        if (node.value < value)
        {
            if (node.right == INVALID_INDEX)
            {
                node.right = newIndex;
                break;
            }

            nodeIndex = node.right;
        }
        else
        {
            if (node.left == INVALID_INDEX)
            {
                node.left = newIndex;
                break;
            }

            nodeIndex = node.left;
        }
    }

    new (m_nodes[newIndex]) Node{nodeIndex, 0, 0, value, Color::RED};

    insert_fix(get_node(newIndex));

    return true;
}

template <typename Type, uint64_t Capacity>
inline void red_black_tree<Type, Capacity>::insert_fix(Node& newNode)
{
    Node& parent = get_node(newNode.parent);
    if (parent.color == Color::RED)
    {
    }
}
} // namespace cxx
} // namespace iox
